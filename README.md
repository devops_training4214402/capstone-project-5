First part of the script

```json
def gv

pipeline {   
    agent any
    stages {
        stage("copy files to ansible server") {
            steps {
                script {
                    echo "copying all necessary files to ansbile control node"                    
                    sshagent(['ansible-server-key']) {
                        sh "scp -o StrictHostKeyChecking=no ansible/* root@64.23.158.147:/root"

                        withCredentials([sshUserPrivateKey(credentialsId: "ec2-server-key", keyFileVariable: 'keyfile', usernameVariable: 'user')]){
                            sh 'scp $keyfile root@64.23.158.147:/root/ssh-key.pem'
                        }
                    }
                }
            }
        }
        stage("execute ansible playbook"){
            steps {
                script {
                    echo " calling ansible playbook to configure ec2 instances"
                    def remote = [:]
                    remote.name = "ansible-server"
                    remote.host = "64.23.158.147"
                    remote.allowAnyHosts = true

                    withCredentials([sshUserPrivateKey(credentialsId: "ansible-server-key", keyFileVariable: 'keyfile', usernameVariable: 'user')]){
                        remote.user = user
                        remote.identityFile = keyfile
                        sshCommand remote: remote, command: "ansible-playbook my-playbook.yaml"

                    }
                }
            }
        }
    }
} 

```

After jenkins configuration fix error

![[Pasted image 20240207222415.png]]


![[Pasted image 20240207222519.png]]



Validation  & Execution
![[Pasted image 20240207223953.png]]

